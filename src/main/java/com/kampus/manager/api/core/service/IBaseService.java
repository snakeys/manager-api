package com.kampus.manager.api.core.service;


import com.kampus.manager.api.core.dto.Dto;

import java.util.List;

public interface IBaseService<T extends Dto,ID> {
    T save (T dto);

    T updateById(T dto, ID id);

    boolean deleteById(ID id);

    T findById(ID id);
}

package com.kampus.manager.api.core.exception;


import com.kampus.manager.api.core.enums.EnmCode;

public class NotUpdatedException extends BaseCustomException {
    public NotUpdatedException(String errorMessage) {
        super(EnmCode.NOT_UPDATED, errorMessage);
    }
}

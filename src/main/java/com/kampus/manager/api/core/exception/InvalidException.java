package com.kampus.manager.api.core.exception;


import com.kampus.manager.api.core.enums.EnmCode;

public class InvalidException extends BaseCustomException {
    public InvalidException(String errorMessage) {
        super(EnmCode.INVALID, errorMessage);
    }
}

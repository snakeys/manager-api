package com.kampus.manager.api.core.exception;


import com.kampus.manager.api.core.enums.EnmCode;

public class NotDeletedException extends BaseCustomException {
    public NotDeletedException(String errorMessage) {
        super(EnmCode.NOT_DELETED, errorMessage);
    }
}

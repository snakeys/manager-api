package com.kampus.manager.api.core.exception;


import com.kampus.manager.api.core.enums.EnmCode;

public class ExistException extends BaseCustomException {
    public ExistException(String errorMessage) {
        super(EnmCode.EXIST, errorMessage);
    }
}

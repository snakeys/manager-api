package com.kampus.manager.api.core.constants;

public class ApiUrlConstants {

    public static final String USER_API_BASE_URL = "http://localhost:9001/api/";
    public static final String U_OGRENCI_API_URL = USER_API_BASE_URL + "ogrenci/";
    public static final String U_OGRENCI_API_SINIF_URL = U_OGRENCI_API_URL+"sinif";
    public static final String U_ROL_API_URL = USER_API_BASE_URL + "rol/";
    public static final String U_KULLANICI_API_URL = USER_API_BASE_URL + "kullanici";
}

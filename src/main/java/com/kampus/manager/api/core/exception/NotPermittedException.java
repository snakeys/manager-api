package com.kampus.manager.api.core.exception;


import com.kampus.manager.api.core.enums.EnmCode;

public class NotPermittedException extends BaseCustomException {
    public NotPermittedException(String errorMessage) {
        super(EnmCode.NOT_PERMITTED, errorMessage);
    }
}

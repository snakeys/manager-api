package com.kampus.manager.api.controller;

import com.kampus.manager.api.core.controller.GenericRestController;
import com.kampus.manager.api.core.dto.ResponseDto;
import com.kampus.manager.api.dto.OkulDto;
import com.kampus.manager.api.service.IOkulService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/okul")
@AllArgsConstructor
public class OkulController extends GenericRestController<OkulDto, String, IOkulService<OkulDto, String>> {

    @Autowired
    IOkulService okulService;

    @Override
    public ResponseDto create(@RequestBody OkulDto dto) {
        final OkulDto okulDto = (OkulDto) okulService.save(dto);
        return ResponseDto.done(getMessageFromSource("success"),okulDto);
    }

    @Override
    public ResponseDto update(@PathVariable("id") String id, @RequestBody OkulDto dto) {
        final OkulDto okulDto = (OkulDto) okulService.updateById(dto,id);
        return ResponseDto.done(getMessageFromSource("success"),okulDto);
    }

    @Override
    public ResponseDto deleteById(@PathVariable("id") String id) {
        final boolean isDeleted = okulService.deleteById(id);
        return ResponseDto.done(getMessageFromSource("success"),isDeleted);
    }

    @Override
    public ResponseDto getById(@PathVariable("id") String id) {
        final OkulDto okulDto = (OkulDto) okulService.findById(id);
        return ResponseDto.done(getMessageFromSource("success"),okulDto);
    }
}

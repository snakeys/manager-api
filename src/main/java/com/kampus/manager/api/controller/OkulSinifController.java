package com.kampus.manager.api.controller;

import com.kampus.manager.api.core.controller.GenericRestController;
import com.kampus.manager.api.core.dto.ResponseDto;
import com.kampus.manager.api.dto.OkulDto;
import com.kampus.manager.api.dto.OkulSinifDto;
import com.kampus.manager.api.entity.OkulSinif;
import com.kampus.manager.api.service.IOkulService;
import com.kampus.manager.api.service.IOkulSinifService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/okul-sinif")
@RequiredArgsConstructor
public class OkulSinifController extends GenericRestController<OkulSinifDto, String, IOkulSinifService<OkulSinifDto, String>> {

    @Autowired
    IOkulSinifService okulSinifService;

    @Override
    public ResponseDto create(@RequestBody OkulSinifDto dto) {
        OkulSinifDto okulSinifDto = (OkulSinifDto) okulSinifService.save(dto);
        return ResponseDto.done(getMessageFromSource("success"),okulSinifDto);
    }

    @Override
    public ResponseDto update(@PathVariable("id") String id,@RequestBody OkulSinifDto dto) {
        OkulSinifDto okulSinifDto = (OkulSinifDto) okulSinifService.updateById(dto,id);
        return ResponseDto.done(getMessageFromSource("success"),okulSinifDto);
    }

    @Override
    public ResponseDto getById(@PathVariable("id") String id) {
        OkulSinifDto okulSinifDto = (OkulSinifDto) okulSinifService.findById(id);
        return ResponseDto.done(getMessageFromSource("success"),okulSinifDto);
    }

    @Override
    public ResponseDto deleteById(String id) {
        boolean result =  okulSinifService.deleteById(id);
        return ResponseDto.done(getMessageFromSource("success"),result);
    }
}

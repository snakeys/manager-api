package com.kampus.manager.api.controller;

import com.kampus.manager.api.core.controller.GenericRestController;
import com.kampus.manager.api.core.dto.ResponseDto;
import com.kampus.manager.api.dto.BransDto;
import com.kampus.manager.api.service.IBransService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/brans")
@AllArgsConstructor
public class BransController extends GenericRestController<BransDto, String, IBransService<BransDto, String>> {

    @Autowired
    IBransService bransService;

    @Override
    public ResponseDto create(@RequestBody BransDto dto) {
        BransDto bransDto = (BransDto) bransService.save(dto);
        return ResponseDto.done(getMessageFromSource("success"), bransDto);
    }

    @Override
    public ResponseDto update(@PathVariable("id") String id, @RequestBody BransDto dto) {
        BransDto bransDto = (BransDto) bransService.updateById(dto, id);
        return ResponseDto.done(getMessageFromSource("success"), bransDto);
    }

    @Override
    public ResponseDto getById(@PathVariable("id") String id) {
        BransDto bransDto = (BransDto) bransService.findById(id);
        return ResponseDto.done(getMessageFromSource("success"), bransDto);
    }

    @Override
    public ResponseDto deleteById(@PathVariable("id") String id) {
        boolean result = bransService.deleteById(id);
        return ResponseDto.done(getMessageFromSource("success"), result);
    }

    @GetMapping
    public ResponseDto getAll(){
        List<BransDto> bransDtos = bransService.getAllBrans();
        return ResponseDto.done(getMessageFromSource("success"),bransDtos);
    }
}

package com.kampus.manager.api.service;

import com.kampus.manager.api.core.dto.Dto;
import com.kampus.manager.api.core.service.IBaseService;
import com.kampus.manager.api.dto.BransDto;

import java.util.List;


public interface IBransService<E extends Dto, I extends String> extends IBaseService<BransDto, String> {

    List<BransDto> getAllBrans();

}

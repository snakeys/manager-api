package com.kampus.manager.api.service;

import com.kampus.manager.api.core.dto.Dto;
import com.kampus.manager.api.core.service.IBaseService;
import com.kampus.manager.api.core.util.TPage;
import com.kampus.manager.api.dto.OkulSinifDto;
import com.kampus.manager.api.dto.filter.OkulSinifFilterDto;
import com.kampus.manager.api.entity.OkulSinif;
import org.springframework.data.domain.Pageable;

public interface IOkulSinifService<E extends Dto, I extends String> extends IBaseService<OkulSinifDto, String> {

        public TPage<OkulSinifDto> findByPagingCriteria(OkulSinifFilterDto okulSinifFilterDto, Pageable pageable);

}

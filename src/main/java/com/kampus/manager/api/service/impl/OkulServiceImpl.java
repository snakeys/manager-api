package com.kampus.manager.api.service.impl;

import com.kampus.manager.api.core.dto.RequestDto;
import com.kampus.manager.api.core.exception.ExistException;
import com.kampus.manager.api.core.exception.NotFoundException;
import com.kampus.manager.api.core.service.BaseService;
import com.kampus.manager.api.dto.OkulDto;
import com.kampus.manager.api.entity.Okul;
import com.kampus.manager.api.enums.EnmOkulTur;
import com.kampus.manager.api.mapper.OkulEntityOkulDtoMapper;
import com.kampus.manager.api.repository.OkulAyarRepository;
import com.kampus.manager.api.repository.OkulRepository;
import com.kampus.manager.api.service.IOkulService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OkulServiceImpl extends BaseService implements IOkulService<RequestDto, String> {

    @Autowired
    OkulRepository okulRepository;
    @Autowired
    OkulEntityOkulDtoMapper okulEntityOkulDtoMapper;

    @Override
    public OkulDto save(OkulDto dto) {
        Optional<Okul> existOkulName = okulRepository.findByAd(dto.getAd());
        if (existOkulName.isPresent()) {
            throw new ExistException("error");
        }
        Okul newOkul = okulEntityOkulDtoMapper.dtoToEntity(dto);
        if (dto.getOkulTurId() < 1) {
            dto.setOkulTurId(EnmOkulTur.OZEL.getId());
        }
        newOkul.setEklemeTarihi(LocalDateTime.now());
        newOkul.setOkulTur(EnmOkulTur.getById(dto.getOkulTurId()));
        okulRepository.save(newOkul);
        dto.setId(newOkul.getId());

        return dto;
    }

    @Override
    public OkulDto updateById(OkulDto dto, String id) {
        Optional<Okul> existOkul = okulRepository.findById(id);
        if (!existOkul.isPresent()) {
            throw new NotFoundException("error");
        }
        if (!existOkul.get().getAd().equals(dto.getAd())){
            existOkul = okulRepository.findByAd(dto.getAd());
            if (existOkul.isPresent()){
                throw new ExistException("error");
            }
        }
        Okul updatedOkul = okulEntityOkulDtoMapper.dtoToEntity(dto);
        if (dto.getOkulTurId() > 0) {
            updatedOkul.setOkulTur(EnmOkulTur.getById(dto.getOkulTurId()));
        }
        okulRepository.save(updatedOkul);
        return dto;

    }

    @Override
    public boolean deleteById(String id) {
        Optional<Okul> existOkul = okulRepository.findById(id);
        if (existOkul.isPresent()) {
          existOkul.get().setSilindiMi(true);
          okulRepository.save(existOkul.get());
            return true;
        }
        throw new NotFoundException("error");
    }

    @Override
    public OkulDto findById(String id) {
        Optional<Okul> existOkul = okulRepository.findById(id);
        if (!existOkul.isPresent()) {
            throw new NotFoundException("error");
        }
        return okulEntityOkulDtoMapper.entityToDto(existOkul.get());
    }

}

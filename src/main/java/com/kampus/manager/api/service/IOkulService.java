package com.kampus.manager.api.service;

import com.kampus.manager.api.core.dto.Dto;
import com.kampus.manager.api.core.service.IBaseService;
import com.kampus.manager.api.dto.OkulDto;

public interface IOkulService<E extends Dto, I extends String> extends IBaseService<OkulDto, String> {

}

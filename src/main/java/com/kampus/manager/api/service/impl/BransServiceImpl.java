package com.kampus.manager.api.service.impl;

import com.kampus.manager.api.core.dto.RequestDto;
import com.kampus.manager.api.core.exception.ExistException;
import com.kampus.manager.api.core.exception.NotFoundException;
import com.kampus.manager.api.core.helper.StringHelper;
import com.kampus.manager.api.core.service.BaseService;
import com.kampus.manager.api.dto.BransDto;
import com.kampus.manager.api.entity.Brans;
import com.kampus.manager.api.mapper.BransEntityBransDtoMapper;
import com.kampus.manager.api.repository.BransRepository;
import com.kampus.manager.api.service.IBransService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BransServiceImpl extends BaseService implements IBransService<RequestDto, String> {

    @Autowired
    BransRepository bransRepository;
    @Autowired
    BransEntityBransDtoMapper bransEntityBransDtoMapper;

    @Override
    public BransDto save(BransDto dto) {
        Optional<Brans> existBrans = bransRepository.findByAdOrKod(dto.getAd(), dto.getKod());
        if (existBrans.isPresent()) {
            throw new ExistException("error");
        }
        dto.setAd(StringHelper.toUpper(dto.getAd()));
        dto.setKod(StringHelper.toUpper(dto.getKod()));
        Brans brans = bransEntityBransDtoMapper.dtoToEntity(dto);
        bransRepository.save(brans);
        dto.setId(brans.getId());
        return dto;
    }

    @Override
    public BransDto updateById(BransDto dto, String id) {
        Optional<Brans> existBrans = bransRepository.findById(id);
        if (!existBrans.isPresent()) {
           throw new NotFoundException("error");
        }
        if (!existBrans.get().getAd().equals(dto.getAd())) {
            Optional<Brans> existName = bransRepository.findByAd(dto.getAd());
            if (existName.isPresent()) {
                throw  new ExistException("error");
            }
        }
        if (!existBrans.get().getAd().equals(dto.getAd())) {
            Optional<Brans> existKod = bransRepository.findByKod(dto.getAd());
            if (existKod.isPresent()) {
                throw  new ExistException("error");
            }
        }
        dto.setAd(StringHelper.toUpper(dto.getAd()));
        dto.setKod(StringHelper.toUpper(dto.getKod()));
        dto.setId(id);
        Brans brans = bransEntityBransDtoMapper.dtoToEntity(dto);
        bransRepository.save(brans);
        return dto;
    }

    @Override
    public boolean deleteById(String id) {
        Optional<Brans> existBrans = bransRepository.findById(id);
        if (!existBrans.isPresent()) {
            throw new NotFoundException("error");
        }
        bransRepository.deleteById(id);
        return true;
    }

    @Override
    public BransDto findById(String id) {
        Optional<Brans> existBrans = bransRepository.findById(id);
        if (!existBrans.isPresent()) {
            throw new NotFoundException("error");
        }
        return bransEntityBransDtoMapper.entityToDto(existBrans.get());
    }

    @Override
    public List<BransDto> getAllBrans() {
       List<Brans> bransList = bransRepository.findAll();
       if (bransList.isEmpty()){
           return null;
       }
       return bransEntityBransDtoMapper.entitiesToDtos(bransList);
    }
}

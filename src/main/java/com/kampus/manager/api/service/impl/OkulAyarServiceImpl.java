package com.kampus.manager.api.service.impl;

import com.kampus.manager.api.core.dto.RequestDto;
import com.kampus.manager.api.core.exception.InvalidException;
import com.kampus.manager.api.core.exception.NotFoundException;
import com.kampus.manager.api.core.service.BaseService;
import com.kampus.manager.api.dto.OkulAyarDto;
import com.kampus.manager.api.entity.Okul;
import com.kampus.manager.api.entity.OkulAyar;
import com.kampus.manager.api.mapper.OkulAyarEntityOkulAyarDtoMapper;
import com.kampus.manager.api.repository.OkulAyarRepository;
import com.kampus.manager.api.repository.OkulRepository;
import com.kampus.manager.api.service.IOkulAyarService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OkulAyarServiceImpl extends BaseService implements IOkulAyarService<RequestDto, String> {

    @Autowired
    OkulAyarRepository okulAyarRepository;
    @Autowired
    OkulAyarEntityOkulAyarDtoMapper okulAyarEntityOkulAyarDtoMapper;
    @Autowired
    OkulRepository okulRepository;

    @Override
    public OkulAyarDto save(OkulAyarDto dto) {
        Optional<Okul> existOkul = okulRepository.findById(dto.getOkulId());
        if (!existOkul.isPresent()) {
            throw new NotFoundException("error");
        }
        if (!okulAyarTimeCheck(dto)) {
            throw new InvalidException("error");
        }
        OkulAyar okulAyar = okulAyarEntityOkulAyarDtoMapper.dtoToEntity(dto);
        okulAyarRepository.save(okulAyar);
        dto.setId(okulAyar.getId());
        return dto;

    }

    @Override
    public OkulAyarDto updateById(OkulAyarDto dto, String id) {
        Optional<OkulAyar> existOkulAyar = okulAyarRepository.findByOkulId(id);
        if (!existOkulAyar.isPresent()) {
            throw new NotFoundException("error");
        }

        if (!updateOkulAyarTimeCheck(existOkulAyar.get(), dto)) {
            throw new InvalidException("error");
        }

        dto.setId(id);
        OkulAyar okulAyar = okulAyarEntityOkulAyarDtoMapper.dtoToEntity(dto);
        okulAyarRepository.save(okulAyar);
        return dto;
    }

    @Override
    public boolean deleteById(String id) {
        Optional<OkulAyar> okulAyar = okulAyarRepository.findById(id);
        if (!okulAyar.isPresent()) {
            throw new NotFoundException("error");
        }
        okulAyarRepository.deleteById(id);
        return true;
    }

    @Override
    public OkulAyarDto findById(String id) {
        Optional<OkulAyar> okulAyar = okulAyarRepository.findById(id);
        if (!okulAyar.isPresent()) {
            throw new NotFoundException("error");
        }

        OkulAyarDto okulAyarDto = okulAyarEntityOkulAyarDtoMapper.entityToDto(okulAyar.get());
        return okulAyarDto;
    }

    private boolean okulAyarTimeCheck(OkulAyarDto dto) {

        if (dto.getDersBaslamaSaat() < 1 && dto.getDersBaslamaSaat() > 23) {
            return false;
        }
        if (dto.getDersBaslamaDakika() < 0 && dto.getDersBaslamaDakika() > 59) {
            return false;
        }
        if (dto.getDersBitisSaat() < 1 && dto.getDersBitisSaat() > 23) {
            return false;
        }
        if (dto.getDersBitisDakika() < 0 && dto.getDersBitisDakika() > 59) {
            return false;
        }
        if (dto.getOgleArasiBaslamaSaat() < 1 && dto.getOgleArasiBaslamaSaat() > 23) {
            return false;
        }
        if (dto.getOgleArasiBaslamaDakika() < 0 && dto.getOgleArasiBaslamaDakika() > 59) {
            return false;
        }
        if (dto.getDersBaslamaSaat() > dto.getDersBitisSaat()) {
            return false;
        }
        if (dto.getDersBaslamaSaat() > dto.getOgleArasiBaslamaSaat()) {
            return false;
        }
        if (dto.getOgleArasiBaslamaSaat() > dto.getDersBitisSaat()) {
            return false;
        }
        return true;
    }

    private boolean updateOkulAyarTimeCheck(OkulAyar okulAyar, OkulAyarDto dto) {
        //Ders başlama
        if (okulAyar.getDersBaslamaSaat() != dto.getDersBaslamaSaat()) {
            if (dto.getDersBaslamaSaat() < 1 && dto.getDersBaslamaSaat() > 23) {
                return false;
            }
            if (okulAyar.getDersBitisSaat() != dto.getDersBitisSaat()) {
                if (dto.getDersBaslamaSaat() > dto.getDersBitisSaat()) {
                    return false;
                }
            }

            if (dto.getDersBaslamaSaat() > okulAyar.getDersBitisSaat()) {
                return false;
            }

        }
        if (okulAyar.getDersBaslamaDakika() != dto.getDersBaslamaDakika()) {
            if (dto.getDersBaslamaDakika() < 0 && dto.getDersBaslamaDakika() > 59) {
                return false;
            }
        }
        //Ders bitiş
        if (okulAyar.getDersBitisSaat() != dto.getDersBitisSaat()) {
            if (dto.getDersBitisSaat() < 1 && dto.getDersBitisSaat() > 23) {
                return false;
            }
            if (okulAyar.getDersBaslamaSaat() != dto.getDersBaslamaSaat()) {
                if (dto.getDersBitisSaat() < dto.getDersBaslamaSaat()) {
                    return false;
                }
            }
            if (okulAyar.getDersBaslamaSaat() > dto.getDersBitisSaat()) {
                return false;
            }
            if (okulAyar.getOgleArasiBaslamaSaat() != dto.getOgleArasiBaslamaSaat()) {
                if (dto.getDersBitisSaat() < dto.getOgleArasiBaslamaSaat()) {
                    return false;
                }
            }
            if (dto.getDersBitisSaat() < okulAyar.getOgleArasiBaslamaSaat()) {
                return false;
            }
        }
        if (okulAyar.getDersBitisDakika() != dto.getDersBitisDakika()) {
            if (dto.getDersBitisDakika() < 0 && dto.getDersBitisDakika() > 59) {
                return false;
            }
        }
        //Ogle arasi
        if (okulAyar.getOgleArasiBaslamaSaat() != dto.getOgleArasiBaslamaSaat()) {
            if (dto.getOgleArasiBaslamaSaat() < 1 && dto.getOgleArasiBaslamaSaat() > 23) {
                return false;
            }
            if (dto.getOgleArasiBaslamaSaat() > okulAyar.getDersBitisSaat()) {
                return false;
            }
            if (dto.getOgleArasiBaslamaSaat() < okulAyar.getDersBaslamaSaat()) {
                return false;
            }
            if (dto.getOgleArasiBaslamaSaat() > okulAyar.getDersBitisSaat()) {
                return false;
            }
        }
        if (okulAyar.getOgleArasiBaslamaDakika() != dto.getOgleArasiBaslamaDakika()) {
            if (dto.getOgleArasiBaslamaDakika() < 0 && dto.getOgleArasiBaslamaDakika() > 59) {
                return false;
            }
        }
        return true;
    }

}

package com.kampus.manager.api.service.impl;

import com.kampus.manager.api.core.dto.RequestDto;
import com.kampus.manager.api.core.exception.ExistException;
import com.kampus.manager.api.core.exception.InvalidException;
import com.kampus.manager.api.core.exception.NotDeletedException;
import com.kampus.manager.api.core.exception.NotFoundException;
import com.kampus.manager.api.core.service.BaseService;
import com.kampus.manager.api.core.util.TPage;
import com.kampus.manager.api.dto.OkulSinifDto;
import com.kampus.manager.api.dto.api.ApiResponseDto;
import com.kampus.manager.api.dto.filter.OkulSinifFilterDto;
import com.kampus.manager.api.entity.OkulSinif;
import com.kampus.manager.api.mapper.OkulSinifEntityOkulSinifDtoMapper;
import com.kampus.manager.api.repository.OkulSinifRepository;
import com.kampus.manager.api.service.IOkulSinifService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

import static com.kampus.manager.api.core.constants.ApiUrlConstants.U_OGRENCI_API_SINIF_URL;

@Service
@RequiredArgsConstructor
public class OkulSinifServiceImpl extends BaseService implements IOkulSinifService<RequestDto, String> {

    @Autowired
    OkulSinifRepository okulSinifRepository;
    @Autowired
    OkulSinifEntityOkulSinifDtoMapper okulSinifDtoMapper;
    @Autowired
    RestTemplate restTemplate;

    @Override
    public OkulSinifDto save(OkulSinifDto dto) {
        Optional<OkulSinif> existOkulSinif = okulSinifRepository.findByOkulIdAndDereceAndSube(dto.getOkulId(), dto.getDerece(), dto.getSube());
        if (existOkulSinif.isPresent()) {
            throw new ExistException("error");
        }
        if (dto.getDerece() < 1 && dto.getDerece() > 12) {
            throw new InvalidException("error");
        }
        if (dto.getSube() == null) {
            throw new InvalidException("error");
        }
        dto.setKod(dto.getDerece() + "-" + dto.getSube());
        OkulSinif okulSinif = okulSinifDtoMapper.dtoToEntity(dto);
        okulSinifRepository.save(okulSinif);
        dto.setId(okulSinif.getId());
        return dto;
    }

    @Override
    public OkulSinifDto updateById(OkulSinifDto dto, String id) {
        Optional<OkulSinif> existOkulSinif = okulSinifRepository.findById(id);
        if (!existOkulSinif.isPresent()) {
            throw new NotFoundException("error");
        }
        if (!existOkulSinif.get().getDerece().equals(dto.getDerece())) {
            if (dto.getDerece() < 1 && dto.getDerece() > 12) {
                throw new InvalidException("error");
            }
        }
        dto.setKod(dto.getDerece() + "-" + dto.getSube());
        OkulSinif okulSinif = okulSinifDtoMapper.dtoToEntity(dto);
        okulSinifRepository.save(okulSinif);
        return dto;
    }

    @Override
    public boolean deleteById(String id) {
        Optional<OkulSinif> existOkulSinif = okulSinifRepository.findById(id);
        if (!existOkulSinif.isPresent()) {
            throw new NotFoundException("error");
        }
        ResponseEntity<ApiResponseDto> responseDto = restTemplate.getForEntity(U_OGRENCI_API_SINIF_URL + id, ApiResponseDto.class);
        if (responseDto.getBody() != null && responseDto.getStatusCode() != HttpStatus.OK) {
            throw new NotDeletedException("error");
        }
        okulSinifRepository.deleteById(id);
        return true;

    }

    @Override
    public OkulSinifDto findById(String id) {
        Optional<OkulSinif> existOkulSinif = okulSinifRepository.findById(id);
        if (!existOkulSinif.isPresent()) {
            throw new NotFoundException("error");
        }

        return okulSinifDtoMapper.entityToDto(existOkulSinif.get());
    }

    @Override
    public TPage<OkulSinifDto> findByPagingCriteria(OkulSinifFilterDto okulSinifFilterDto, Pageable pageable) {
        List<OkulSinif> okulSinifList = okulSinifRepository.findByPagingCriteria(okulSinifFilterDto, pageable);
        List<OkulSinifDto> data = okulSinifDtoMapper.entitiesToDtos(okulSinifList);
        TPage<OkulSinifDto> response = new TPage<>();
        response.setResult(data);
        return response;
    }
}

package com.kampus.manager.api.mapper;

import com.kampus.manager.api.dto.OkulAyarDto;
import com.kampus.manager.api.entity.OkulAyar;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OkulAyarEntityOkulAyarDtoMapper {

    OkulAyar dtoToEntity(OkulAyarDto dto);

    OkulAyarDto entityToDto(OkulAyar okulAyar);
}

package com.kampus.manager.api.mapper;


import com.kampus.manager.api.dto.BransDto;
import com.kampus.manager.api.entity.Brans;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BransEntityBransDtoMapper {

    Brans dtoToEntity(BransDto dto);

    BransDto entityToDto(Brans okul);

    List<BransDto> entitiesToDtos(List<Brans> bransList);
}

package com.kampus.manager.api.mapper;

import com.kampus.manager.api.dto.OkulSinifDto;
import com.kampus.manager.api.entity.OkulSinif;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OkulSinifEntityOkulSinifDtoMapper {

    OkulSinif dtoToEntity(OkulSinifDto dto);

    OkulSinifDto entityToDto(OkulSinif okulSinif);

    List<OkulSinifDto> entitiesToDtos(List<OkulSinif> okulSinifList);
}

package com.kampus.manager.api.mapper;

import com.kampus.manager.api.dto.OkulDto;
import com.kampus.manager.api.entity.Okul;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OkulEntityOkulDtoMapper {

    @Mapping(target = "okulTur",ignore = true)
    @Mapping(target = "eklemeTarihi",ignore = true)
    Okul dtoToEntity(OkulDto dto);

    @Mapping(target = "okulTurId",ignore = true)
    OkulDto entityToDto(Okul okul);
}

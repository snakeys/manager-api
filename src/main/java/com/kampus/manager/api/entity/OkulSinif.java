package com.kampus.manager.api.entity;


import com.kampus.manager.api.core.BaseEntity;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "okul_sinif")
public class OkulSinif extends BaseEntity {
    @Field("okul_id")
    public String okulId;
    @Field("derece")
    public Integer derece;
    @Field("sube")
    public String sube;
    @Field("kod")
    public String kod;
}

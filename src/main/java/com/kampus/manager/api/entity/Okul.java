package com.kampus.manager.api.entity;

import com.kampus.manager.api.core.BaseEntity;
import com.kampus.manager.api.enums.EnmOkulTur;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "okul")
public class Okul extends BaseEntity {

    @Field("ad")
    private String ad;
    @Field("okul_tur")
    private EnmOkulTur okulTur;
    @Field("telefon")
    private String telefon;
    @Field("ozel_mi")
    private boolean ozelMi;
    @Field("il_id")
    private Integer ilId;
    @Field("ilce_id")
    private Integer ilceId;
    @Field("adres")
    private String adres;
    @Field("web_sitesi")
    private String webSitesi;
    @Field("email")
    private String email;
    @Field("ekleme_tarihi")
    private LocalDateTime eklemeTarihi;
    @Field("silindi_mi")
    private boolean silindiMi;

}

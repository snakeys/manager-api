package com.kampus.manager.api.entity;

import com.kampus.manager.api.core.BaseEntity;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "okul_ayar")
public class OkulAyar extends BaseEntity {

    @Field("okul_id")
    private String okulId;
    @Field("ders_baslama_saat")
    private Integer dersBaslamaSaat;
    @Field("ders_baslama_dakika")
    private Integer dersBaslamaDakika;
    @Field("ders_bitis_saat")
    private Integer dersBitisSaat;
    @Field("ders_bitis_dakika")
    private Integer dersBitisDakika;
    @Field("ders_uzunlugu")
    private Integer dersUzunlugu;
    @Field("tenefus_uzunlugu")
    private Integer tenefusUzunlugu;
    @Field("ogle_arasi_baslama_saat")
    private Integer ogleArasiBaslamaSaat;
    @Field("ogle_arasi_baslama_dakika")
    private Integer ogleArasiBaslamaDakika;
    @Field("ogle_arasi_uzunlugu")
    private Integer ogleArasiUzunlugu;
}

package com.kampus.manager.api.entity;

import com.kampus.manager.api.core.BaseEntity;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "brans")
public class Brans extends BaseEntity {
    @Field("ad")
    public String ad;
    @Field("kod")
    public String kod;
}

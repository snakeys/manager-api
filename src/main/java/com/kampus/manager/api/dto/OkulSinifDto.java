package com.kampus.manager.api.dto;

import com.kampus.manager.api.core.dto.RequestDto;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OkulSinifDto extends RequestDto {
    public String okulId;
    public Integer derece;
    public String sube;
    public String kod;
}


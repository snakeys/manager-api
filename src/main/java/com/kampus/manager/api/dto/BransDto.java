package com.kampus.manager.api.dto;

import com.kampus.manager.api.core.dto.RequestDto;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BransDto extends RequestDto {
    String ad;
    String kod;
}

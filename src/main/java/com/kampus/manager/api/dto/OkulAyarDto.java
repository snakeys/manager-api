package com.kampus.manager.api.dto;

import com.kampus.manager.api.core.dto.RequestDto;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OkulAyarDto extends RequestDto {

    public Integer dersBaslamaSaat;
    public Integer dersBaslamaDakika;
    public Integer dersBitisSaat;
    public Integer dersBitisDakika;
    public Integer dersUzunlugu;
    public Integer tenefusUzunlugu;
    public Integer ogleArasiBaslamaSaat;
    public Integer ogleArasiBaslamaDakika;
    public Integer ogleArasiUzunlugu;
    public String okulId;
}

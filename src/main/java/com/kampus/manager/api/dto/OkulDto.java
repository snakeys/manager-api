package com.kampus.manager.api.dto;

import com.kampus.manager.api.core.dto.RequestDto;
import lombok.*;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class OkulDto extends RequestDto {
    private String ad;
    private Integer okulTurId;
    private String telefon;
    private boolean ozelMi;
    private Integer ilId;
    private Integer ilceId;
    private String adres;
    private String webSitesi;
    private String email;
    private LocalDateTime eklemeTarihi;
    private boolean silindiMi = false;
}

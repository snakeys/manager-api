package com.kampus.manager.api.dto.filter;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class OkulSinifFilterDto {
    String okulId;
    String derece;
    String sube;

}

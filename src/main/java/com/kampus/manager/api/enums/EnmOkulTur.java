package com.kampus.manager.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum EnmOkulTur {

    OZEL(1, "ILKOKUL"),
    DEVLET(2, "LISE");

    private Integer id;
    private String ad;

    public static EnmOkulTur getById(int id) {
        for (EnmOkulTur okulTur : EnmOkulTur.values()
        ) {
            if (id == okulTur.id) {
                return okulTur;
            }
        }
        return null;
    }
}

package com.kampus.manager.api.repository;

import com.kampus.manager.api.core.BaseRepository;
import com.kampus.manager.api.entity.Brans;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BransRepository extends BaseRepository<Brans> {

    Optional<Brans> findByAdOrKod(String ad,String kod);
    Optional<Brans> findByAd(String ad);
    Optional<Brans> findByKod(String kod);

}

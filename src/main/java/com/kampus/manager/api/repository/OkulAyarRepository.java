package com.kampus.manager.api.repository;

import com.kampus.manager.api.core.BaseRepository;
import com.kampus.manager.api.entity.OkulAyar;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OkulAyarRepository extends BaseRepository<OkulAyar> {

    @Query(value = "{'okulId': $0}", delete = true)
    public void deleteByOkulId(String okulId);

    Optional<OkulAyar> findByOkulId(String okulId);
}

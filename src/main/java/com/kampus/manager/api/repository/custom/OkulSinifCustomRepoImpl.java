package com.kampus.manager.api.repository.custom;

import com.kampus.manager.api.core.helper.StringHelper;
import com.kampus.manager.api.dto.filter.OkulSinifFilterDto;
import com.kampus.manager.api.entity.OkulSinif;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class OkulSinifCustomRepoImpl implements OkulSinifCustomRepo {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<OkulSinif> findByPagingCriteria(OkulSinifFilterDto filterDto, Pageable pageable) {
        final Query query = new Query().with(pageable);
        final List<Criteria> criteria = new ArrayList<>();

        if (!StringHelper.isNullOrEmpty(filterDto.getOkulId())) {
            criteria.add(Criteria.where("okulId").is(filterDto.getOkulId()));
        }
        if (!StringHelper.isNullOrEmpty(filterDto.getDerece())) {
            criteria.add(Criteria.where("derece").is(filterDto.getDerece()));
        }
        if (!StringHelper.isNullOrEmpty(filterDto.getOkulId())) {
            criteria.add(Criteria.where("sube").is(filterDto.getSube()));
        }

        if (!criteria.isEmpty()) {
            query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
        }
        List<OkulSinif> okulSinifList = mongoTemplate.find(query, OkulSinif.class);
        return okulSinifList;

    }
}

package com.kampus.manager.api.repository;

import com.kampus.manager.api.core.BaseRepository;
import com.kampus.manager.api.entity.OkulSinif;
import com.kampus.manager.api.repository.custom.OkulSinifCustomRepo;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OkulSinifRepository extends BaseRepository<OkulSinif>, OkulSinifCustomRepo {
    Optional<OkulSinif> findByOkulIdAndDereceAndSube(String okulId, Integer derece,String sube);

}

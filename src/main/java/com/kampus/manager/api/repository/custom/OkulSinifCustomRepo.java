package com.kampus.manager.api.repository.custom;

import com.kampus.manager.api.dto.filter.OkulSinifFilterDto;
import com.kampus.manager.api.entity.OkulSinif;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OkulSinifCustomRepo {
    public List<OkulSinif> findByPagingCriteria(OkulSinifFilterDto filterDto, Pageable pageable);
}

package com.kampus.manager.api.repository;

import com.kampus.manager.api.core.BaseRepository;
import com.kampus.manager.api.entity.Okul;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OkulRepository extends BaseRepository<Okul> {
    Optional<Okul> findByAd(String ad);

}
